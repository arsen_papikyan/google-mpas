<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "maps".
 *
 * @property integer $id
 * @property string $coordinates
 * @property string $description
 */
class Maps extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'maps';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['coordinates'], 'string', 'max' => 500],
            [['description'], 'string', 'max' => 300],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'coordinates' => 'Coordinates',
            'description' => 'Description',
        ];
    }
}
