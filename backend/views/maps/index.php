<?php

use yii\helpers\Html;
use backend\modules\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\MpasControl */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Maps';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="maps-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Show Maps', ['#'], [
            'class' => 'btn btn-success',
            'id' => 'btn_maps',
            'data-is_valid_map' => "noValid"
        ]) ?>
    </p>
    <div class="col-xs-12 showGridView">
        <?= GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],
                'coordinates',
                'description',

                ['class' => 'yii\grid\ActionColumn',
                    'template'=>'{view}{update}',
                ],
            ],
            "panel" => [
                "before" => ""
            ]
        ]); ?>

    </div>

    <?php

    $this->registerCss("
    #show_maps{
    display: none;
    width: 100%;
    height: 500px;
}

")
    ?>
    <div class="col-xs-12">
        <div id="show_maps"></div>
    </div>

</div>
