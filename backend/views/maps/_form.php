<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\Maps */
/* @var $form yii\widgets\ActiveForm */

$this->registerCss("#map{
    width: 100%;
    height: 600px;
}")
?>

<div class="maps-form">

    <?php $form = ActiveForm::begin(); ?>

        <?= $form->field($model, 'coordinates')->hiddenInput() ?>

    <?= $form->field($model, 'description')->textInput() ?>
    <div id="map"></div>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
